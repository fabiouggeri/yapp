/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.decision;

/**
 *
 * @author fabio_uggeri
 */
public class IndirectCondition implements Condition {
   
   private Condition condition;

   public IndirectCondition(Condition condition) {
      this.condition = condition;
   }

   public IndirectCondition() {
      this(null);
   }

   public void setCondition(Condition condition) {
      this.condition = condition;
   }

   public Condition getCondition() {
      return condition;
   }

   @Override
   public boolean isTrue() {
      return condition != null && condition.isTrue();
   }
}
