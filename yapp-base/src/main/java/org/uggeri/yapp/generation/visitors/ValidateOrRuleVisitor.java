/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.generation.visitors;

import org.uggeri.yapp.generation.ParserGenerationOptions;
import org.uggeri.yapp.grammar.GrammarRuleVisitor;
import org.uggeri.yapp.grammar.rules.AndRule;
import org.uggeri.yapp.grammar.rules.AnyCharRule;
import org.uggeri.yapp.grammar.rules.CharRangeRule;
import org.uggeri.yapp.grammar.rules.CharRule;
import org.uggeri.yapp.grammar.rules.EOIRule;
import org.uggeri.yapp.grammar.rules.EmptyRule;
import org.uggeri.yapp.grammar.rules.GrammarRule;
import org.uggeri.yapp.grammar.rules.IgnoreCaseCharRule;
import org.uggeri.yapp.grammar.rules.IgnoreCaseStringRule;
import org.uggeri.yapp.grammar.rules.NonTerminalRule;
import org.uggeri.yapp.grammar.rules.OneOrMoreRule;
import org.uggeri.yapp.grammar.rules.OptionalRule;
import org.uggeri.yapp.grammar.rules.OrRule;
import org.uggeri.yapp.grammar.rules.StringRule;
import org.uggeri.yapp.grammar.rules.TestNotRule;
import org.uggeri.yapp.grammar.rules.TestRule;
import org.uggeri.yapp.grammar.rules.ZeroOrMoreRule;

/**
 *
 * @author fabio_uggeri
 */
public class ValidateOrRuleVisitor implements GrammarRuleVisitor {

   private boolean invalidOr = false;

   @Override
   public void visitNonTerminal(ParserGenerationOptions options, NonTerminalRule rule) {
   }

   @Override
   public void visitAnd(ParserGenerationOptions options, AndRule rule) {
      if (!invalidOr) {
         for (GrammarRule subRule : rule.getRules()) {
            subRule.visit(options, this);
         }
      }
   }

   @Override
   public void visitOr(ParserGenerationOptions options, OrRule rule) {
      if (!invalidOr) {
         boolean optional = false;
         for (GrammarRule subRule : rule.getRules()) {
            if (optional) {
               invalidOr = true;
               break;
            }
            if (subRule.getOptional().isTrue()) {
               optional = true;
            }
         }
      }
   }

   @Override
   public void visitChar(ParserGenerationOptions options, CharRule rule) {
   }

   @Override
   public void visitCharIgnoreCase(ParserGenerationOptions options, IgnoreCaseCharRule rule) {
   }

   @Override
   public void visitCharRange(ParserGenerationOptions options, CharRangeRule rule) {
   }

   @Override
   public void visitAnyChar(ParserGenerationOptions options, AnyCharRule rule) {
   }

   @Override
   public void visitString(ParserGenerationOptions options, StringRule rule) {
   }

   @Override
   public void visitStringIgnoreCase(ParserGenerationOptions options, IgnoreCaseStringRule rule) {
   }

   @Override
   public void visitZeroOrMore(ParserGenerationOptions options, ZeroOrMoreRule rule) {
   }

   @Override
   public void visitOneOrMore(ParserGenerationOptions options, OneOrMoreRule rule) {
   }

   @Override
   public void visitTest(ParserGenerationOptions options, TestRule rule) {
      if (!invalidOr) {
         rule.getRule().visit(options, this);
      }
   }

   @Override
   public void visitTestNot(ParserGenerationOptions options, TestNotRule rule) {
      if (!invalidOr) {
         rule.getRule().visit(options, this);
      }
   }

   @Override
   public void visitOptional(ParserGenerationOptions options, OptionalRule rule) {
   }

   @Override
   public void visitEmpty(ParserGenerationOptions options, EmptyRule rule) {
   }

   @Override
   public void visitEOI(ParserGenerationOptions options, EOIRule rule) {
   }

   public boolean hasInvalidOr() {
      return invalidOr;
   }
}
