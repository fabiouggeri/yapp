/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.decision;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fabio_uggeri
 */
public class AndCondition implements Condition {

   private final List<Condition> subconditions = new ArrayList<Condition>();

   public List<Condition> getSubconditions() {
      return subconditions;
   }
 
   public void AddCondition(Condition condition) {
      subconditions.add(condition);
   }
   
   @Override
   public boolean isTrue() {
      for (Condition c : subconditions) {
         if (! c.isTrue()) {
            return false;
         }
      }
      return true;
   }
}
