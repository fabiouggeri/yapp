#include <stdlib.h>
#include "yapp_visitor.h"

static void yapp_visitor_dummy(YAPP_NODE *node) {
}

YAPP_VISITOR* yapp_visitor_new(YAPP_BUFFER *buffer, INT32 rulesCount) {
   YAPP_VISITOR* visitor = (YAPP_VISITOR *) malloc(sizeof(YAPP_VISITOR) + (sizeof(RULE_VISITOR) * rulesCount * 2));
   INT32 i;
   INT32 len = rulesCount * 2;
   visitor->buffer = buffer;
   visitor->rulesCount = rulesCount;
   visitor->data = NULL;
   for (i = 0; i < len; i++) {
      visitor->rulesVisitor[i] = yapp_visitor_dummy;
   }
   return visitor;
}

void yapp_visitor_free(YAPP_VISITOR *visitor) {
   free(visitor);
}

void yapp_visitor_enterRule(YAPP_VISITOR *visitor, YAPP_NODE *node) {
   visitor->rulesVisitor[node->rule->value](visitor, node);
}

void yapp_visitor_exitRule(YAPP_VISITOR *visitor, YAPP_NODE *node) {
   visitor->rulesVisitor[node->rule->value + visitor->rulesCount](visitor, node);
}

void yapp_visitor_setEnterRule(YAPP_VISITOR *visitor, YAPP_RULE *rule, RULE_VISITOR enterVisitor) {
   visitor->rulesVisitor[rule->value] = enterVisitor;
}

void yapp_visitor_setExitRule(YAPP_VISITOR *visitor, YAPP_RULE *rule, RULE_VISITOR exitVisitor) {
   visitor->rulesVisitor[rule->value + visitor->rulesCount] = exitVisitor;
}

void yapp_visitor_treeWalker(YAPP_VISITOR *visitor, YAPP_NODE *node) {
   YAPP_NODE *child = node->firstChild;
   yapp_visitor_enterRule(visitor, node);
   while (child != NULL) {
      yapp_visitor_treeWalker(visitor, child);
      child = child->sibling;
   }
   yapp_visitor_exitRule(visitor, node);
}