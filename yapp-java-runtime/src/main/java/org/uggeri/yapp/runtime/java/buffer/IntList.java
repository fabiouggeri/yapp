/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.runtime.java.buffer;

import java.util.Arrays;

/**
 *
 * @author fabio_uggeri
 */
public class IntList {

   public static class UnderflowException extends RuntimeException {

      public UnderflowException(String message) {
         super(message);
      }
   }

   private static final int INITIAL_CAPACITY = 16;
   private int[] array;
   private int size;

   public IntList(int iniCap) {
      array = new int[iniCap < 0 ? INITIAL_CAPACITY : iniCap];
      size = 0;
   }

   public IntList() {
      this(INITIAL_CAPACITY);
   }

   /**
    * Tests if the stack is empty.
    *
    * @return true if empty, false otherwise.
    */
   public boolean isEmpty() {
      return size == 0;
   }

   /**
    * Returns the number of element currently on the stack.
    *
    * @return the number of element currently on the stack
    */
   public int size() {
      return size;
   }

   /**
    * Empties the stack.
    */
   public void clear() {
      size = 0;
   }

   /**
    * Returns the item at the top of the stack without removing it.
    *
    * @param index index of element to get
    * @return the most recently inserted item in the stack.
    * @throws UnderflowException if the stack is empty.
    */
   public int get(int index) {
      if (index < 0 || index >= size) {
         throw new ArrayIndexOutOfBoundsException("IntList get");
      }
      return array[index];
   }

   /**
    * Removes the most recently inserted item from the stack.
    *
    * @param index index of element to remove
    *
    * @return the top stack item
    * @throws UnderflowException if the stack is empty.
    */
   public int remove(int index) {
      final int oldValue;
      final int lenMove;
      if (index < 0 || index >= size) {
         throw new ArrayIndexOutOfBoundsException("IntList get");
      }
      oldValue = array[index];
      lenMove = size - index - 1;
      if (lenMove > 0) {
         System.arraycopy(array, index + 1, array, index, lenMove);
      }
      --size;
      return oldValue;
   }

   /**
    * Pushes a new item onto the stack.
    *
    * @param x the item to add.
    */
   public void add(int x) {
      if (size == array.length) {
         expandCapacity();
      }
      array[size++] = x;
   }

   private void expandCapacity() {
      final int[] newArray = new int[(array.length >> 1) + 1 + array.length];
      System.arraycopy(array, 0, newArray, 0, array.length);
      array = newArray;
   }

   public int binarySearch(final int key) {
      int low = 0;
      int high = size - 1;

      while (low <= high) {
         final int mid = (low + high) >>> 1;
         final int midVal = array[mid];

         if (midVal < key) {
            low = mid + 1;
         } else if (midVal > key) {
            high = mid - 1;
         } else {
            return mid; // key found
         }
      }
      return -(low + 1);  // key not found.
   }
}
