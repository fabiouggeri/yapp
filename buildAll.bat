SET GOAL=%1

IF "%GOAL%"=="" SET GOAL=install

call mvn clean %GOAL% -Drelease -DskipTest
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO

cd yapp-c-runtime

call mvn %GOAL% -Drelease -DskipTest -Darch=32
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO_MODULO

call mvn %GOAL% -Drelease -DskipTest -Dcc=mingw64
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO_MODULO

call mvn %GOAL% -Drelease -DskipTest -Dcc=mingw64 -Darch=32
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO_MODULO

call mvn %GOAL% -Drelease -DskipTest -Dcc=clang
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO_MODULO

call mvn %GOAL% -Drelease -DskipTest -Dcc=clang -Darch=32
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO_MODULO

call mvn %GOAL% -Drelease -DskipTest -Dcc=bcc
IF NOT "%ERRORLEVEL%"=="0" GOTO ERRO_MODULO

:ERRO_MODULO
cd ..

GOTO FIM

:ERRO
echo "Error building YAPP"

:FIM