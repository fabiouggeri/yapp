#ifndef _YAPP_NODE_

#define _YAPP_NODE_

#include "yapp_defs.h"
#include "yapp_buffer.h"
#include "yapp_rule.h"
#include "yapp_mem_manager.h"

typedef struct _YAPP_NODE {
   YAPP_RULE* rule;
   BOOL semantic;
   BOOL skiped;
   INT32 startIndex;
   INT32 endIndex;
   void *value;
   struct _YAPP_NODE *nextNode;
   struct _YAPP_NODE *firstChild;
   struct _YAPP_NODE *sibling;
} YAPP_NODE;

extern YAPP_NODE * yapp_node_newManaged(YAPP_MEM_MANAGER *memManager, YAPP_RULE *rule, INT32 startIndex, INT32 endIndex, BOOL semantic, BOOL skipNode);
extern YAPP_NODE * yapp_node_new(YAPP_RULE *rule, INT32 startIndex, INT32 endIndex, BOOL semantic, BOOL skipNode);
extern void yapp_node_free(YAPP_NODE *node);
extern void yapp_node_freeTree(YAPP_NODE *node);
extern void * yapp_node_getValue(YAPP_NODE *node);
extern void yapp_node_setValue(YAPP_NODE *node, void *value);
extern YAPP_RULE * yapp_node_getRule(YAPP_NODE *node);
extern INT32 yapp_node_getStartIndex(YAPP_NODE *node);
extern void yapp_node_setStartIndex(YAPP_NODE *node, INT32 index);
extern INT32 yapp_node_getEndIndex(YAPP_NODE *node);
extern void yapp_node_setEndIndex(YAPP_NODE *node, INT32 index);
extern INT32 yapp_node_getLength(YAPP_NODE *node);
extern char * yapp_node_getText(YAPP_NODE *node, YAPP_BUFFER *buffer);
extern char * yapp_node_getTextPointer(YAPP_NODE *node, YAPP_BUFFER *buffer);
extern YAPP_NODE* yapp_node_getSemanticSibling(YAPP_NODE *node);
extern YAPP_NODE* yapp_node_getSibling(YAPP_NODE *node);
extern void yapp_node_setSibling(YAPP_NODE *node, YAPP_NODE *sibling);
extern YAPP_NODE* yapp_node_getFirstSemanticChild(YAPP_NODE *node);
extern YAPP_NODE* yapp_node_getFirstChild(YAPP_NODE *node);
extern void yapp_node_setFirstChild(YAPP_NODE *node, YAPP_NODE *child);
extern YAPP_NODE** yapp_node_getChildren(YAPP_NODE *node);
extern YAPP_NODE** yapp_node_getSemanticChildren(YAPP_NODE *node);
extern BOOL yapp_node_isSemantic(YAPP_NODE *node);
extern BOOL yapp_node_isSkiped(YAPP_NODE *node);
extern void yapp_node_printTree(YAPP_BUFFER *buffer, YAPP_NODE *node);
extern void yapp_node_printSemanticTree(YAPP_BUFFER *buffer, YAPP_NODE *node);

#endif
