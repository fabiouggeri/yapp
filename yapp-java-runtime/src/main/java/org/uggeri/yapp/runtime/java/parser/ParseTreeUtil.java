/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.runtime.java.parser;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import org.uggeri.yapp.runtime.java.buffer.InputBuffer;
import org.uggeri.yapp.runtime.java.node.Node;
import java.util.List;

/**
 *
 * @author fabio
 */
public class ParseTreeUtil {
   
   public static void printTree(InputBuffer inputBuffer, Node node) {
      printTree(inputBuffer, node, 0, System.out);
   }
   
   public static void printSemanticTree(InputBuffer inputBuffer, Node node) {
      printSemanticTree(inputBuffer, node, 0, System.out);
   }
   
   public static String toString(InputBuffer inputBuffer, Node node) {
      final ByteArrayOutputStream out = new ByteArrayOutputStream(inputBuffer.length());
      final PrintStream print = new PrintStream(out);
      printSemanticTree(inputBuffer, node, 0, print);
      return out.toString();
   }
   
   private static void printTree(InputBuffer inputBuffer, Node node, int i, PrintStream out) {
      for (int j = 0; j < i; j++) {
         out.print("   ");
      }
      out.print('[');
      out.print(node.getRule().getLabel());
      out.print("] '");
      out.print(node.getText(inputBuffer));
      out.println('\'');
      for (Node child: (List<Node>)node.getChildren()) {
         printTree(inputBuffer, child, i + 1, out);
      }
   }
  
   private static void printSemanticTree(InputBuffer inputBuffer, Node node, int i, PrintStream out) {
      for (int j = 0; j < i; j++) {
         out.print("   ");
      }
      out.print('[');
      out.print(node.getRule().getLabel());
      out.print("] '");
      out.print(node.getText(inputBuffer));
      out.println('\'');
      for (Node child: (List<Node>)node.getSemanticChildren()) {
         printSemanticTree(inputBuffer, child, i + 1, out);
      }
   }
}
