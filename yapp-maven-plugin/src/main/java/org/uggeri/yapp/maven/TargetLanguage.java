/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.maven;

/**
 *
 * @author fabio
 */
public enum TargetLanguage {
   JAVA {
      @Override
      public String toString() {
         return "java";
      }
      
   },
   C {
      @Override
      public String toString() {
         return "c";
      }
      
   };   
}
