/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.decision;

/**
 *
 * @author fabio_uggeri
 */
public class SingleCondition implements Condition {

   private boolean value;

   public SingleCondition(boolean value) {
      this.value = value;
   }

   public void setValue(boolean value) {
      this.value = value;
   }
   
   @Override
   public boolean isTrue() {
      return value;
   }
   
}
