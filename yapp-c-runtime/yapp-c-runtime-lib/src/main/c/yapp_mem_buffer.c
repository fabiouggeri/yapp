#include <stdlib.h>
#include <string.h>
#include "yapp_mem_buffer.h"

typedef struct _MEM_BUFFER_DATA  {
   char *buffer;
   INT32 dataLength;
} MEM_BUFFER_DATA;


static void freeData(void *bufferData) {
   free(bufferData);
}

static INT32 lengthData(void *bufferData) {
   return ((MEM_BUFFER_DATA *)bufferData)->dataLength;
}

static char getChar(void *bufferData, INT32 index) {
   if (index >= 0 && index < ((MEM_BUFFER_DATA *)bufferData)->dataLength) {
      char c = ((MEM_BUFFER_DATA *)bufferData)->buffer[index];
      return c;
   }
   return '\0';
}

static char* getText(void *bufferData, INT32 startIndex, INT32 endIndex) {
   if (endIndex > startIndex && startIndex >= 0 && endIndex <= ((MEM_BUFFER_DATA *)bufferData)->dataLength) {
      INT32 len = endIndex - startIndex;
      char *text = (char *) malloc(sizeof(char) * (len + 1));
      memcpy(text, &(((MEM_BUFFER_DATA *)bufferData)->buffer[startIndex]), sizeof(char) * len);
      text[len] = '\0';
      return text;
   }
   return NULL;
}

static char* getTextPointer(void *bufferData, INT32 index) {
   if (index >= 0 && index < ((MEM_BUFFER_DATA *)bufferData)->dataLength) {
      return &(((MEM_BUFFER_DATA *)bufferData)->buffer[index]);
   }
   return NULL;
}

YAPP_BUFFER* yapp_mem_buffer_newLen(const char *str, INT32 len) {
   YAPP_BUFFER *buffer = yapp_buffer_new();
   MEM_BUFFER_DATA *data = malloc(sizeof(MEM_BUFFER_DATA));
   data->buffer = str;
   data->dataLength = len;
   buffer->data = data;
   buffer->getText = getText;
   buffer->getTextPointer = getTextPointer;
   buffer->getChar = getChar;
   buffer->length = lengthData;
   buffer->free = freeData;
   return buffer;
}

YAPP_BUFFER* yapp_mem_buffer_new(const char *str) {
   return yapp_mem_buffer_newLen(str, strlen(str));
}
