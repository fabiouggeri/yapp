/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.decision;

/**
 *
 * @author fabio_uggeri
 */
public interface Condition {
   
   public static final Condition TRUE = new Condition() {
      @Override
      public boolean isTrue() {
         return true;
      }
   };
   
   public static final Condition FALSE = new Condition() {
      @Override
      public boolean isTrue() {
         return false;
      }
   };
   
   public boolean isTrue();
}
