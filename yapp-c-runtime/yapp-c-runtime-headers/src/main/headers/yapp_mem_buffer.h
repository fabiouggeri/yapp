#ifndef _YAPP_MEM_BUFFER_

#define _YAPP_MEM_BUFFER_

#include "yapp_buffer.h"

YAPP_BUFFER* yapp_mem_buffer_newLen(const char *str, INT32 len);
YAPP_BUFFER* yapp_mem_buffer_new(const char *str);

#endif

