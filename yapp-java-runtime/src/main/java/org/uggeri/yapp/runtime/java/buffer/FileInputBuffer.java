/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uggeri.yapp.runtime.java.buffer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 *
 * @author fabio
 */
public class FileInputBuffer extends AbstractInputBuffer {

   private static final int DEFAULT_BUFFER_SIZE = 4096;//8192;

   private int bufferSize;

   private File file;
   
   private final CharSequence buffer;
   
   private final Charset charset;
   
   public FileInputBuffer(final File file, final int bufferSize, final Charset fileCharset) throws IOException {
      this.file = file;
      this.bufferSize = bufferSize;
      this.charset = fileCharset == null ? Charset.defaultCharset() : fileCharset;
      this.buffer = readFileContent(file);
   }
   
   public FileInputBuffer(final File file, final int bufferSize, final String charset) throws IOException {
      this(file, bufferSize, Charset.forName(charset));
   }
   
   public FileInputBuffer(final File file) throws IOException {
      this(file, DEFAULT_BUFFER_SIZE, Charset.defaultCharset());
   }
   
   public FileInputBuffer(final File file, final String charset) throws IOException {
      this(file, DEFAULT_BUFFER_SIZE, Charset.forName(charset));
   }
   
   public FileInputBuffer(final File file, final Charset charset) throws IOException {
      this(file, DEFAULT_BUFFER_SIZE, charset);
   }
   
   public File getFile() {
      return file;
   }

   @Override
   protected CharSequence getBuffer() {
      return buffer;
   }

   private CharSequence readFileContent(File file) throws IOException {
      InputStreamReader reader = null;
      StringBuilder fileContent;
      try {
         final char[] chunk = new char[this.bufferSize];
         int charsRead;
         reader = new InputStreamReader(new FileInputStream(file), this.charset);
         fileContent = new StringBuilder((int)file.length());
         charsRead = reader.read(chunk);
         while (charsRead >= 0) {
            fileContent.append(chunk, 0, charsRead);
            charsRead = reader.read(chunk);
         }
      } finally {
         if (reader != null) {
            reader.close();
         }
      }
      return fileContent;
   }
   
}