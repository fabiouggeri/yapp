#ifndef _YAPP_DEFS_

#define _YAPP_DEFS_

#if (defined(_MSC_VER) && _MSC_VER >= 1600) || (defined(__BORLANDC__) && __BORLANDC__ >= 0x0600) || defined(__GNUC__)
#include <stdint.h>
#else
   #define int8_t              signed __int8
   #define int16_t             signed __int16
   #define int32_t             signed __int32
   #define int64_t             signed __int64
   #define uint8_t             unsigned __int8
   #define uint16_t            unsigned __int16
   #define uint32_t            unsigned __int32
   #define uint64_t            unsigned __int64
#endif

#ifndef NULL
   #define NULL 0
#endif

#ifndef BOOL
   #define BOOL int
#endif

#ifndef TRUE
   #define TRUE 1
#endif

#ifndef FALSE
   #define FALSE 0
#endif

#ifndef INT32 
   #if defined( __NT__ ) || defined( __WINDOWS_386__ ) || defined( __WINDOWS__ )
      #define INT32 __int32
   #else
      #define INT32 int32_t
   #endif
#endif

#ifndef INT64
   #if defined( __NT__ ) || defined( __WINDOWS_386__ ) || defined( __WINDOWS__ )
      #define INT64 __int64
   #else
      #define INT64 int64_t
   #endif
#endif

#ifndef BYTE 
   #define BYTE unsigned char 
#endif

#endif
